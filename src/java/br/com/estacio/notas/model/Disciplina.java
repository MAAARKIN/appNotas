/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.estacio.notas.model;

public class Disciplina {
    
    private String nome;
    
    private String nota1;
    
    private String nota2;
    
    private String nota3;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNota1() {
        return nota1;
    }

    public void setNota1(String nota1) {
        this.nota1 = nota1;
    }

    public String getNota2() {
        return nota2;
    }

    public void setNota2(String nota2) {
        this.nota2 = nota2;
    }

    public String getNota3() {
        return nota3;
    }

    public void setNota3(String nota3) {
        this.nota3 = nota3;
    }  

    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj instanceof Disciplina) {
            Disciplina disciplina = (Disciplina) obj;
            if (disciplina.getNome() == this.nome) {
                return true;
            }
            if (disciplina.getNome() != null && disciplina.getNome().equals(this.nome)) {
                return true;
            }
        }
        
        return false;
    }
    
}
