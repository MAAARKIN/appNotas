/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.estacio.notas.model;

import java.util.ArrayList;
import java.util.List;

public class Aluno {
    private String nome;
    private int idade;
    private String matricula;
    private float media;
    private List<Disciplina> disciplinasMatriculadas;

    public float getMedia() {
        return media;
    }

    public void setMedia(float media) {
        this.media = media;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public List<Disciplina> getDisciplinasMatriculadas() {
        return disciplinasMatriculadas;
    }

    public void setDisciplinasMatriculadas(List<Disciplina> disciplinasMatriculadas) {
        this.disciplinasMatriculadas = disciplinasMatriculadas;
    }
    
    public void adicinarDisciplina(Disciplina disciplina) {
        if (disciplinasMatriculadas == null) {
            disciplinasMatriculadas = new ArrayList<>();
        }
        disciplinasMatriculadas.add(disciplina);
    }
  
    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj instanceof Aluno) {
            Aluno a = (Aluno) obj;
            if (a.getMatricula() == this.matricula) {
                return true;
            }
            if (a.getMatricula() != null && a.getMatricula().equals(this.matricula)) {
                return true;
            }
        }
        return false;
    }
    
}
