/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.estacio.notas.servlet;

import br.com.estacio.notas.model.Aluno;
import br.com.estacio.notas.model.Disciplina;
import br.com.estacio.notas.repositorio.Alunos;
import br.com.estacio.notas.repositorio.Disciplinas;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "MatriculaServlet", urlPatterns = {"/matricular"})
public class MatriculaServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setAttribute("matricula", request.getParameter("matricula"));
        request.setAttribute("disciplina", request.getParameter("disciplina"));

        request.getRequestDispatcher("/matriculaForm.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String nomeDisciplina = request.getParameter("disciplina");
        String matriculaAluno = request.getParameter("matriculaAluno");

        Aluno aluno = Alunos.getAlunoPorMatricula(matriculaAluno);
        Disciplina disciplina = Disciplinas.getDisciplinaPorNome(nomeDisciplina);

        aluno.adicinarDisciplina(disciplina);

        Alunos.update(aluno);
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet NewServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Matricula realizada com sucesso!</h1>");
            out.println("<a href='/AppNotas'>Inicio</a>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
