<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Alunos do sistema</h1>
        <table style="width:100%">
            <tr>
                <th>Nome</th>
                <th>Idade</th> 
                <th>Matricula</th>
                <th>Media Geral</th>
                <th>Operacoes</th>
            </tr>
            <c:forEach items="${alunos}" var="aluno">
            <tr>
                <td align="center">${aluno.nome}</td>
                <td align="center">${aluno.idade}</td> 
                <td align="center">${aluno.matricula}</td>
                <td align="center">${aluno.media}</td>
                <td align="center"> 
<!--                    <a href="/AppNotas/addNotas?matricula=${aluno.matricula}">notas</a> -->
                    <a href="/AppNotas/disciplinasNotas?matricula=${aluno.matricula}">Disciplinas Mat.</a> 
                    | 
                    <a href="/AppNotas/removerAluno?matricula=${aluno.matricula}">Remover</a>
                </td>
            </tr>
            </c:forEach>
        </table>
        <br/>
        <a href="/AppNotas">Voltar</a>
    </body>
</html>
