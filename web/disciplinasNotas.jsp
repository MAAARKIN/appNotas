<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Disciplinas matriculadas para o aluno de matricula: ${matricula}</h1>
        <table style="width:100%">
            <tr>
                <th>Nome</th>
                <th>Acao</th>
            </tr>
            <c:forEach items="${disciplinas}" var="disciplina">
            <tr>
                <td align="center">${disciplina.nome}</td>
               <td align="center"> 
                   <a href="/AppNotas/addNotas?matricula=${matricula}&disciplina=${disciplina.nome}">Notas</a> </td>
            </tr>
            </c:forEach>
        </table>
        <br/>
        <a href="/AppNotas">Voltar</a>
    </body>
</html>
