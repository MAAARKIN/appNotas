<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Add Notas - Matricula do aluno: ${matricula}</h1>
        
            <h1>Disciplinas matriculadas</h1>
            <table style="width:100%">
                <tr>
                     <th>Nome</th>
                     <th>Nota 1</th>
                     <th>Nota 2</th>
                     <th>Nota 3</th>
                     <th>Acao</th>
                </tr>
                <c:forEach items="${disciplinas}" var="d">
                <tr>
                    <td align="center">${d.nome}</td>
                    <td align="center">${d.nota1}</td>
                    <td align="center">${d.nota2}</td>
                    <td align="center">${d.nota3}</td>
                     <td align="center"><a href="/AppNotas/addNotas?matricula=${matricula}&d=${d.nome}">adicionar/editar notas</a></td>
                </tr>
                </c:forEach>
            </table>
            <input type="submit" value="Salvar" />
        </form>
    </body>
</html>
