<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Disciplina: ${disciplina}</h1>
        <form method="POST" action="matricular">
            Matricula do aluno:
            <input type="hidden" name="disciplina" id="disciplina" value="${disciplina}">
            <input type="text" name="matriculaAluno" />
            <input type="submit" value="Matricular na disciplina" />
        </form>
        <br/><a href='/AppNotas'>Inicio</a>
    </body>
</html>
