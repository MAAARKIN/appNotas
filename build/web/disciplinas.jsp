<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Disciplinas do sistema</h1>
        <table style="width:100%">
            <tr>
                <th>Nome</th>
                <th>Acao</th>
            </tr>
            <c:forEach items="${disciplinas}" var="disciplina">
            <tr>
                <td align="center">${disciplina.nome}</td>
                <td align="center">  <a href="/AppNotas/removerDisciplina?disciplinaNome=${disciplina.nome}">Remover</a>
                |
<!--                  <a href="matriculaForm.jsp?disciplina=${disciplina.nome}">matricular aluno</a></td>-->
                <a href="/AppNotas/matricular?disciplina=${disciplina.nome}">Matricular aluno</a></td>
            </tr>
            </c:forEach>
        </table>
        <br/>
        <a href="/AppNotas">Voltar</a>
    </body>
</html>
